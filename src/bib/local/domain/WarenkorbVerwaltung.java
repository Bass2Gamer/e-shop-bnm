package bib.local.domain;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Warenkorb;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class WarenkorbVerwaltung {
    private Warenkorb warenkorb;

    public WarenkorbVerwaltung() {
        this.warenkorb = new Warenkorb();
    }

    public void addArtikel(Artikel artikel, int quantity) {
        ArrayList<Artikel> artikels = warenkorb.getArtikelInCart();
        ArrayList<Integer> quantities = warenkorb.getQuantities();

        if (artikels.indexOf(artikel) > -1) {
            changeQuantity(artikel.getNummer(), quantity + quantities.get(artikels.indexOf(artikel)));
        } else {
            if (quantity > 0) {
                artikels.add(artikel);
                quantities.add(quantity);
            }
            else {
                System.err.println("Menge muss größer als 0 sein!");
            }
        }
    }

    public void changeQuantity(UUID uuid, int newQuantity) {
        Artikel artikel = findArtikel(uuid);
        ArrayList<Artikel> artikels = warenkorb.getArtikelInCart();
        ArrayList<Integer> quantities = warenkorb.getQuantities();

        if (artikel != null) {
            int index = artikels.indexOf(artikel);
            if (newQuantity > 0) {
                quantities.set(index, newQuantity);
            } else {
                artikels.remove(index);
                quantities.remove(index);
            }
        } else {
            System.err.println("Artikel nicht im Warenkorb");
        }
    }

    public void clear() {
        warenkorb.getArtikelInCart().clear();
        warenkorb.getQuantities().clear();
    }

    public void buy() {
        clear();
    }

    private Artikel findArtikel(UUID uuid) {
        Artikel artikel = null;

        for (Artikel a : warenkorb.getArtikelInCart()) {
            if (a.getNummer().equals(uuid)) {
                artikel = a;
            }
        }
        return artikel;
    }

    public Warenkorb getWarenkorb() {
        return warenkorb;
    }


}
