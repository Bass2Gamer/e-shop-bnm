package bib.local.domain;

import bib.local.domain.exceptions.ArtikelExistiertBereitsException;
import bib.local.domain.exceptions.LoginFehlgeschlagenException;
import bib.local.persistence.FilePersistenceManager;
import bib.local.persistence.PersistenceManager;
import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Mitarbeiter;
import bib.local.valueobjects.User;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.Vector;


public class UserVerwaltung {

	// Verwaltung des Buchbestands in einem Vector
	private List<User> users = new Vector<User>();

	// Persistenz-Schnittstelle, die für die Details des Dateizugriffs verantwortlich ist
	private PersistenceManager pm = new FilePersistenceManager();

	public UserVerwaltung()  {
//		users.add(new Kunde(1, "Peter", "passwort", "Straße", "2", "breee"));
//		users.add(new Mitarbeiter(1, "Ralf", "passwort"));
	}


	public User logIn(String benutzerName, String passWord) throws LoginFehlgeschlagenException {
		User user = sucheUserName(benutzerName);
		if (user != null ) {
			if (user.getPassword().equals(passWord)) {
				return user;
			}
		}

		throw new LoginFehlgeschlagenException(benutzerName); //todo login fehlgeschlagen exception
	}

	public void liesDaten(String datei, String datei2) throws IOException {
		// PersistenzManager für Lesevorgänge öffnen
		pm.openForReading(datei);

		User user;
		do {
			user = pm.ladeKunde();
			if (user != null) {
				// Buch in Liste einfügen
				try {
					einfuegen(user);
				} catch (Exception e1) {
					// Kann hier eigentlich nicht auftreten,
					// daher auch keine Fehlerbehandlung...
				}
			}
		} while (user != null);

		// Persistenz-Schnittstelle wieder schließen
		pm.close();

		pm.openForReading(datei2);

		do {
			user = pm.ladeMitarbeiter();
			if (user != null) {
				// Buch in Liste einfügen
				try {
					einfuegen(user);
					System.out.println("Funktioniert");
				} catch (Exception e1) {
					// Kann hier eigentlich nicht auftreten,
					// daher auch keine Fehlerbehandlung...
				}
			}
		} while (user != null);

		// Persistenz-Schnittstelle wieder schließen
		pm.close();
	}


	public void schreibeDatenM(String datei) throws IOException  {
		pm.openForWriting(datei);

		for (User b : users) {
			if (b instanceof Mitarbeiter) {
				pm.speicherDaten(b.getData());
			}
		}

		pm.close();
	}

	public void schreibeDatenK(String datei) throws IOException  {
		pm.openForWriting(datei);

		for (User b : users) {
			if (b instanceof Kunde) {
				pm.speicherDaten(b.getData());
			}
		}

		pm.close();
	}
		

	public void einfuegen(User user) throws ArtikelExistiertBereitsException {
		if (!users.contains(user)) {
			//throw new BuchExistiertBereitsException(artikel, " - in 'einfuegen()'");
			users.add(user);
		}

		// das übernimmt der Vector:

	}


	public void loeschen(UUID uuid) {
		users.remove(sucheUser(uuid));
	}


	public User sucheUser(UUID uuid) {

		List<User> suchErg = new Vector<User>();

		Iterator<User> iter = users.iterator();
		while (iter.hasNext()) {

			User u = iter.next();
			if (u.getUserNr().equals(uuid))
				suchErg.add(u);
		}
 		if (suchErg.isEmpty()) {
 			return null;
		} else {
			return suchErg.get(0);
		}
	}

	public User sucheUserName(String name) {

		List<User> suchErg = new Vector<User>();

		Iterator<User> iter = users.iterator();
		while (iter.hasNext()) {

			User u = iter.next();
			if (u.getName().equals(name))
				suchErg.add(u);
		}
		if (suchErg.isEmpty()) {
			return null;
		} else {
			return suchErg.get(0);
		}
	}

	public List<User> getUserList() {
		return users;
	}
}
