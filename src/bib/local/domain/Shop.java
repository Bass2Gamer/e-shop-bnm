package bib.local.domain;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

import bib.local.domain.exceptions.ArtikelExistiertBereitsException;
import bib.local.domain.exceptions.ArtikelUnterBestandException;
import bib.local.domain.exceptions.LoginFehlgeschlagenException;
import bib.local.valueobjects.*;


public class Shop {

	private String datei = "";
	
	private ArtikelVerwaltung artikelVerwaltung;

	private UserVerwaltung userVerwaltung;

	private WarenkorbVerwaltung warenkorbVerwaltung;

	public User currentUser;

	public List<String> actions = new Vector<String>();

	public Shop(String datei) throws IOException {
		this.datei = datei;

		artikelVerwaltung = new ArtikelVerwaltung();
		userVerwaltung = new UserVerwaltung();
		warenkorbVerwaltung = new WarenkorbVerwaltung();
		ladeDaten();
		sortiereArtikel("nr");
	}
	public void logIn(String benutzerName, String passWord) throws LoginFehlgeschlagenException {
		currentUser = null;
		currentUser =  userVerwaltung.logIn(benutzerName, passWord);
		System.out.println(currentUser.getData());
	}

	public void logOut() {
		currentUser = null;
	}


	public List<Artikel> gibAlleArtikel() {
		return artikelVerwaltung.getArtikelBestand();
	}

	public List<User> gibAlleUser() {
		return userVerwaltung.getUserList();
	}

	public Artikel sucheNachArtikel(UUID uuid) {
		// einfach delegieren an BuecherVerwaltung meineBuecher
		return artikelVerwaltung.sucheArtikel(uuid);
	}

	public User sucheNachUser(UUID uuid) {
		// einfach delegieren an BuecherVerwaltung meineBuecher
		return userVerwaltung.sucheUser(uuid);
	}

	public Artikel fuegeArtikelEin(String bez, int bestand, double preis) throws ArtikelExistiertBereitsException {
		Artikel b = new Artikel(bez,  preis, bestand);
		artikelVerwaltung.einfuegen(b);
		return b;
	}

	public Artikel fuegeArtikelEin(String bez, int bestand, double preis, int einheit) throws ArtikelExistiertBereitsException {
		Massengutartikel b = new Massengutartikel(bez, preis, bestand, einheit);
		artikelVerwaltung.einfuegen(b);
		return b;
	}

	public User fuegeUserEin(String name, String strasse, String plz, String wohnort, String password) throws ArtikelExistiertBereitsException {
		Kunde b = new Kunde(name,strasse, plz, wohnort, password);
		userVerwaltung.einfuegen(b);
		return b;
	}

	public User fuegeMtiarbeiterEIn(String name, String passwort) throws ArtikelExistiertBereitsException {
		Mitarbeiter b = new Mitarbeiter( name, passwort);
		userVerwaltung.einfuegen(b);
		return b;
	}


	public void loescheBenutzer(UUID uuid) {
		userVerwaltung.loeschen(uuid);
	}


	public void loescheArtikel(UUID uuid) {
		artikelVerwaltung.loeschen(uuid);
	}


	public void schreibeDaten() throws IOException {
		artikelVerwaltung.schreibeDaten(datei+"Artikel.txt");
		userVerwaltung.schreibeDatenM(datei+"Mitarbeiter.txt");
		userVerwaltung.schreibeDatenK(datei+"Kunde.txt");
//		artikelVerwaltung.schreibeActions(datei+"Actions.txt", actions);
	}

	public void ladeDaten() throws IOException {
		userVerwaltung.liesDaten(datei+"Kunde.txt", datei+"Mitarbeiter.txt");
		artikelVerwaltung.liesDaten(datei+"Artikel.txt");
	}

	public void artikelInWarenkorb(Artikel a, int abzug) throws ArtikelUnterBestandException {
		warenkorbVerwaltung.addArtikel(a, abzug);
		artikelVerwaltung.artikelBestandSetzen(a, abzug);
	}

	public void sortiereArtikel(String sortierArt) {
		artikelVerwaltung.sortiereArtikel(sortierArt);
	}

	public Rechnung warenKorbRechnungErstellen(Kunde k) {
		//artikelVerwaltung.artikelBestandAendern(warenkorbVerwaltung.getWarenkorb());
		Rechnung r = new Rechnung(k , warenkorbVerwaltung.getWarenkorb());
		//warenkorbVerwaltung.buy();
		/*String action = java.time.LocalDateTime.now()+"#" + k.getName();
		for (int i = 1; i <= r.getArtikels().size()+1; i++) {
			action += "#"+ r.getArtikels().get(i).getBezeichnung() + "#" + r.getQuantities().get(i);
			System.out.println(action);
		}

		actions.add(action);*/
		return r;
	}

	public void warenkorbKaufen() {
		//artikelVerwaltung.artikelBestandAendern(warenkorbVerwaltung.getWarenkorb());
		//Rechnung r = new Rechnung(k , warenkorbVerwaltung.getWarenkorb());
		warenkorbVerwaltung.buy();
		/*String action = java.time.LocalDateTime.now()+"#" + k.getName();
		for (int i = 1; i <= r.getArtikels().size()+1; i++) {
			action += "#"+ r.getArtikels().get(i).getBezeichnung() + "#" + r.getQuantities().get(i);
			System.out.println(action);
		}

		actions.add(action);*/
	}

	public List<User> getAllUser() {
		return userVerwaltung.getUserList();
	}

	public Warenkorb getWarenkorb() {
		return warenkorbVerwaltung.getWarenkorb();
	}


}
