package bib.local.domain.exceptions;

import bib.local.valueobjects.Artikel;

/**
 * Exception zur Signalisierung, dass ein Buch bereits existiert (z.B. bei einem Einfügevorgang).
 *
 * @author teschke
 */
public class ArtikelUnterBestandException extends Exception {

    private Artikel buch;

    /**
     * Konstruktor
     *
     * @param buch Der bertoffene Artikel
     */
    public ArtikelUnterBestandException(Artikel buch) {
        super("Bestand von " + buch + " ist zu niedirg für diese Opertion!");
        this.buch = buch;
    }

    public Artikel getBuch() {
        return buch;
    }
}
