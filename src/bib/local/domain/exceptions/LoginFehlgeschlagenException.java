package bib.local.domain.exceptions;

import bib.local.valueobjects.Artikel;

/**
 * Exception zur Signalisierung, dass ein Buch bereits existiert (z.B. bei einem Einfügevorgang).
 * 
 * @author teschke
 */
public class LoginFehlgeschlagenException extends Exception {

	private String username;

	/**
	 * Konstruktor
	 *
	 */
	public LoginFehlgeschlagenException(String username) {
		super("Login für Username " + username + " fehlgeschlagen.");
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public String toString() {
		return "Login für Username " + username + " fehlgeschlagen.";
	}
}
