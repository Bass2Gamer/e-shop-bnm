package bib.local.domain.exceptions;

import bib.local.valueobjects.Artikel;

/**
 * Exception zur Signalisierung, dass ein Buch bereits existiert (z.B. bei einem Einfügevorgang).
 * 
 * @author teschke
 */
public class ArtikelExistiertNichtException extends Exception {

	private Artikel buch;

	/**
	 * Konstruktor
	 *
	 * @param buch Das bereits existierende Buch
	 * @param zusatzMsg zusätzlicher Text für die Fehlermeldung
	 */
	public ArtikelExistiertNichtException(Artikel buch, String zusatzMsg) {
		super("Buch mit Titel " + buch.getBezeichnung() + " und Nummer " + buch.getNummer()
				+ " existiert bereits" + zusatzMsg);
		this.buch = buch;
	}

	public Artikel getBuch() {
		return buch;
	}
}
