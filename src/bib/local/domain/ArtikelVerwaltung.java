package bib.local.domain;

import java.io.IOException;
import java.util.*;

import bib.local.domain.exceptions.ArtikelExistiertBereitsException;
import bib.local.domain.exceptions.ArtikelUnterBestandException;
import bib.local.persistence.FilePersistenceManager;
import bib.local.persistence.PersistenceManager;
import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Warenkorb;


/**
 * Klasse zur Verwaltung von Büchern.
 * 
 * @author teschke
 * @version 3
 * - Verwaltung der Bücher in List/Vector mit Generics
 * - außerdem Einsatz von Interfaces (List): Vector ist nur eine mögliche Implementierung!
 */
public class ArtikelVerwaltung {

	// Verwaltung des Buchbestands in einem Vector
	private List<Artikel> artikelBestand = new Vector<Artikel>();

	// Persistenz-Schnittstelle, die für die Details des Dateizugriffs verantwortlich ist
	private PersistenceManager pm = new FilePersistenceManager();

	public ArtikelVerwaltung() {
//		artikelBestand.add(new Artikel("computer", 1000, 10, 100));
//		artikelBestand.add(new Artikel("stift", 1000, 10, 100));
	}


	public void liesDaten(String datei) throws IOException {
		// PersistenzManager für Lesevorgänge öffnen
		pm.openForReading(datei);

		Artikel artikel;
		do {
			// Buch-Objekt einlesen
			artikel = pm.ladeArtikel();
			if (artikel != null) {
				// Buch in Liste einfügen
				try {
					einfuegen(artikel);
				} catch (ArtikelExistiertBereitsException e1) {
					// Kann hier eigentlich nicht auftreten,
					// daher auch keine Fehlerbehandlung...
				}
			}
		} while (artikel != null);

		// Persistenz-Schnittstelle wieder schließen
		pm.close();
	}

	public void artikelBestandSetzen(Artikel a, int abzug) throws ArtikelUnterBestandException{
		if (a.getBestand() - abzug < 0) {
			throw new ArtikelUnterBestandException(a);
		} else {
			a.setBestand(a.getBestand() - abzug);
		}

	}

	public void artikelBestandAendern(Warenkorb warenkorb) {
		for (int i = 0; i < warenkorb.getArtikelInCart().size(); i++) {
			Artikel a = sucheArtikel(warenkorb.getArtikelInCart().get(i).getNummer());
			int newQuant = a.getBestand() - warenkorb.getQuantities().get(i);
			a.setBestand(newQuant);
			if (newQuant < 1) {
				loeschen(a.getNummer());
			}
		}
	}

	public void schreibeDaten(String datei) throws IOException  {
		// PersistenzManager für Schreibvorgänge öffnen
		pm.openForWriting(datei);

		for (Artikel a : artikelBestand) {
			pm.speicherDaten(a.getData());
		}

		pm.close();
	}

	public void schreibeActions(String datei, List<String> actions) throws IOException {
		pm.openForReading(datei);
		for (String s : actions) {
			pm.speicherDaten(s);
		}
	}
		

	public void einfuegen(Artikel artikel) throws ArtikelExistiertBereitsException {
		if (artikelBestand.contains(artikel)) {
			throw new ArtikelExistiertBereitsException(artikel, " - in 'einfuegen()'");
		}

		// das übernimmt der Vector:
		artikelBestand.add(artikel);
	}


	public void loeschen(UUID uuid) {
		Artikel suchErg = sucheArtikel(uuid);

			artikelBestand.remove(suchErg);


	}


	public Artikel sucheArtikel(UUID uuid) {

		List<Artikel> suchErg = new Vector<Artikel>();


		Iterator<Artikel> iter = artikelBestand.iterator();
		while (iter.hasNext()) {

			Artikel b = iter.next();
			if (b.getNummer().equals(uuid))
				suchErg.add(b);
		}
		return suchErg.get(0);
	}

	public List<Artikel> sortiereArtikel(String sortierArt) {
			List<Artikel> sortArtikel = artikelBestand;
			switch(sortierArt) {
				case "nr":
					sortArtikel.sort(Comparator.comparing(Artikel::getNummer));
					return sortArtikel;
				case "bez":
					sortArtikel.sort(Comparator.comparing(Artikel::getBezeichnung));
					return sortArtikel;
			};
			return sortArtikel;

	}
	

	public List<Artikel> getArtikelBestand() {
		return new Vector<Artikel>(artikelBestand);
	}

}
