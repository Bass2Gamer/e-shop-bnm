package bib.local.persistence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.StandardSocketOptions;
import java.util.UUID;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Massengutartikel;
import bib.local.valueobjects.Mitarbeiter;

/**
 * @author teschke
 *
 * Realisierung einer Schnittstelle zur persistenten Speicherung von
 * Daten in Dateien.
 * @see bib.local.persistence.PersistenceManager
 */
public class FilePersistenceManager implements PersistenceManager {

	private BufferedReader reader = null;
	private PrintWriter writer = null;
	
	public void openForReading(String datei) throws FileNotFoundException {
		reader = new BufferedReader(new FileReader(datei));
	}

	public void openForWriting(String datei) throws IOException {
		writer = new PrintWriter(new BufferedWriter(new FileWriter(datei)));
	}

	public boolean close() {
		if (writer != null)
			writer.close();
		
		if (reader != null) {
			try {
				reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				
				return false;
			}
		}

		return true;
	}

	public Artikel ladeArtikel() throws IOException {

		String input = liesZeile();
		if (input == null) {
			return null;
		}
		String[] out = input.split("#");
		if (out.length == 4) {
			return new Artikel(out[0],Double.parseDouble(out[2]), Integer.parseInt(out[3]));
		} else {
			return new Massengutartikel(out[0], Double.parseDouble(out[2]), Integer.parseInt(out[3]), Integer.parseInt(out[4]));
		}

	}


	public Kunde ladeKunde() throws IOException {

		String input = liesZeile();
		if (input == null) {
			return null;
		}
		String[] out = input.split("#");


			return new Kunde(out[1], out[2], out[3] , out[4], out[5]);


	}

	public Mitarbeiter ladeMitarbeiter() throws IOException {
		String input = liesZeile();
		if (input == null) {
			return null;
		}
		String[] out = input.split("#");

		return new Mitarbeiter( out[1], out[2]);
	}



	public boolean speicherDaten(String string) throws IOException {

			schreibeZeile(string);


		return false;
	}


	private String liesZeile() throws IOException {
		if (reader != null)
			return reader.readLine();
		else
			return "";
	}

	private void schreibeZeile(String daten) {
		if (writer != null)
			writer.println(daten);
	}
}
