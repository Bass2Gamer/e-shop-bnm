package bib.local.persistence;

import java.io.IOException;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Mitarbeiter;

/**
 * @author teschke
 *
 * Allgemeine Schnittstelle für den Zugriff auf ein Speichermedium
 * (z.B. Datei oder Datenbank) zum Ablegen von beispielsweise
 * Bücher- oder Kundendaten.
 * 
 * Das Interface muss von Klassen implementiert werden, die eine
 * Persistenz-Schnittstelle realisieren wollen.
 */
public interface PersistenceManager {

	public void openForReading(String datenquelle) throws IOException;
	
	public void openForWriting(String datenquelle) throws IOException;
	
	public boolean close();

	public Artikel ladeArtikel()throws IOException;

	public Kunde ladeKunde()throws IOException;
	public Mitarbeiter ladeMitarbeiter()throws IOException;
	public boolean speicherDaten(String strings)throws IOException;
}