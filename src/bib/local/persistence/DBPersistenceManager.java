package bib.local.persistence;

import java.io.IOException;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Mitarbeiter;

public class DBPersistenceManager implements PersistenceManager {


	@Override
	public void openForReading(String datenquelle) throws IOException {

	}

	@Override
	public void openForWriting(String datenquelle) throws IOException {

	}

	@Override
	public boolean close() {
		return false;
	}

	@Override
	public Artikel ladeArtikel() throws IOException {
		return null;
	}

	@Override
	public Kunde ladeKunde() throws IOException {
		return null;
	}

	@Override
	public Mitarbeiter ladeMitarbeiter() throws IOException {
		return null;
	}

	@Override
	public boolean speicherDaten(String strings) throws IOException {
		return false;
	}
}
