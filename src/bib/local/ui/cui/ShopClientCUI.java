package bib.local.ui.cui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;

import bib.local.domain.WarenkorbVerwaltung;
import bib.local.domain.exceptions.ArtikelExistiertBereitsException;
import bib.local.domain.Shop;
import bib.local.domain.exceptions.LoginFehlgeschlagenException;
import bib.local.valueobjects.*;


/**
 * Klasse für sehr einfache Benutzungsschnittstelle für die Bibliothek.
 * Die Benutzungsschnittstelle basiert auf Ein- und Ausgaben auf der Kommandozeile,
 * daher der Name CUI (Command line User Interface).
 *
 * @author teschke
 * @version 3 (Verwaltung der Bücher in List/Vector mit Generics)
 */
public class ShopClientCUI {

/*	private Shop shop;
	private WarenkorbVerwaltung warenkorbVerwaltung;
	private BufferedReader in;
	private User user;
	private String menueState = "main";
	
	public ShopClientCUI(String datei) throws IOException {
		// die Bib-Verwaltung erledigt die Aufgaben, 
		// die nichts mit Ein-/Ausgabe zu tun haben
		warenkorbVerwaltung = new WarenkorbVerwaltung();
		//shop = new Shop(datei, warenkorbVerwaltung);

		//gibLoginAus();

		// Stream-Objekt fuer Texteingabe ueber Konsolenfenster erzeugen
		in = new BufferedReader(new InputStreamReader(System.in));
	}

	*//* (non-Javadoc)
	 * 
	 * Interne (private) Methode zur Ausgabe des Menüs.
	 *//*
	private void gibKundeMenueAus() {
		System.out.print("Befehle: \n  Artikel ausgeben:  'a'");
		System.out.print("         \n  Artikel suchen:  'f'");
		System.out.print("         \n  Warenkorb verwaltung:  'w'");
		System.out.print("         \n  ---------------------");
		System.out.print("         \n  Ausloggen:        'log'");
		System.out.println("         \n  Beenden:        'q'");
		System.out.print("> "); // Prompt
		//System.out.flush(); // ohne NL ausgeben
	}

	private void gibMitarbeiterMenueAus() {
		System.out.print("Befehle: \n  Artikel ausgeben:  'a'");
		System.out.print("         \n  Artikel löschen:  'd'");
		System.out.print("         \n  Artikel einfügen: 'e'");
		System.out.print("         \n  Artikel suchen:  'f'");
		System.out.print("         \n  Artikel sortieren:  'sort'");
		System.out.print("		   \n  User ausgeben:  'ua'");
		System.out.print("         \n  User löschen:  'ud'");
		System.out.print("         \n  User einfügen: 'ue'");
		System.out.print("         \n  User suchen:  'uf'");
		System.out.print("         \n  ---------------------");
		System.out.print("         \n  Ausloggen:        'log'");
		System.out.println("         \n  Beenden:        'q'");
		System.out.print("> "); // Prompt
		//System.out.flush(); // ohne NL ausgeben
	}

	private void gibLoginAus() {
		System.out.print("         \n  Einloggen: 'e'");
		System.out.print("         \n  Registrieren: 'r'");
		System.out.print("         \n  ---------------------");
		System.out.println("         \n  Beenden:        'q'");
		System.out.print("> "); // Prompt
		menueState = "login";
	}

	private void gibWarenkorbMenuAus() {
		System.out.print("         \n  Warenkorb ausgeben: 'a'");
		System.out.print("         \n  Artikel hinzufügen: 'h'");
		System.out.print("         \n  Artikel menge ändern: 'm'");
		System.out.print("         \n  Warenkorb leeren: 'l'");
		System.out.print("         \n  ---------------------");
		System.out.println("         \n  Beenden:        'q'");
		System.out.print("> "); // Prompt
	}

	*//* (non-Javadoc)
	 *
	 * Interne (private) Methode zum Einlesen von Benutzereingaben.
	 *//*
	private String liesEingabe() throws IOException {
		// einlesen von Konsole
		return in.readLine();
	}

	private int liesEingabeInt() throws IOException {
		// einlesen von Konsole
		String input = in.readLine();
		int number;
		try {
			number = Integer.parseInt(input);
			return number;
		} catch (Exception e) {
			System.err.println("Keine gültige zahl angegeben");
		}

		return -1;
	}



	*//* (non-Javadoc)
	 * 
	 * Interne (private) Methode zur Verarbeitung von Eingaben
	 * und Ausgabe von Ergebnissen.
	 *//*
	private void verarbeiteEingabe(String line) throws IOException, ArtikelExistiertBereitsException {
		String nummer;
		int nr;
		String bez;
		double preis;
		int bestand;
		List<Artikel> liste;
		List<User> listeUser;
		User tempUser;
		String password;
		String username;
		String sortart;


		if (menueState.equals("kunde")) {

			// Eingabe bearbeiten:
			switch (line) {
				case "a":
					liste = shop.gibAlleArtikel();
					gibArtikellisteAus(liste);
					break;

				case "f":
					System.out.print("Artikelbezeichung  > ");
					bez = liesEingabe();
					liste = shop.sucheNachArtikel(bez);
					gibArtikellisteAus(liste);
					break;
				case "w":
					menueState = "warenkorb";
					gibWarenkorbMenuAus();
					verarbeiteEingabe(liesEingabe());
					break;
				case "log":
					System.out.print("Erfolgreich Ausgeloggt!");
					menueState = "login";
					gibLoginAus();
					break;

			}

			if (menueState.equals("kunde")) gibKundeMenueAus();
		} else if (menueState.equals("mitarbeiter")) {

			switch (line) {
				case "a":
					liste = shop.gibAlleArtikel();
					gibArtikellisteAus(liste);
					break;
				case "d":
					// lies die notwendigen Parameter, einzeln pro Zeil
					System.out.print("Artikelbezeichnung  > ");
					bez = liesEingabe();
					// Die Bibliothek das Buch löschen lassen:
					shop.loescheArtikel(bez);
					break;
				case "e":
					// lies die notwendigen Parameter, einzeln pro Zeile
					System.out.print("Artikelbezeichung  > ");
					bez = liesEingabe();
					System.out.print("Artiekl preis  > ");
					preis = Double.parseDouble(liesEingabe());
					System.out.print("Artikelbestand  > ");
					bestand = Integer.parseInt(liesEingabe());

					try {
						shop.fuegeArtikelEin(bez, bestand, preis);
						System.out.println("Einfügen ok");
					} catch (ArtikelExistiertBereitsException e) {
						// Hier Fehlerbehandlung...
						System.out.println("Fehler beim Einfügen");
						e.printStackTrace();
					}
					break;
				case "f":
					System.out.print("Artikelbezeichung  > ");
					bez = liesEingabe();
					liste = shop.sucheNachArtikel(bez);
					gibArtikellisteAus(liste);
					break;
				case "sort":
					System.out.print("Sorieren nach? (nr, bez)  > ");
					sortart = liesEingabe();
					shop.sortiereArtikel(sortart);
					break;
				case "ua":
					listeUser = shop.gibAlleUser();
					gibUserlisteAus(listeUser);
					break;
				case "ud":
					// lies die notwendigen Parameter, einzeln pro Zeile

					System.out.print("Username  > ");
					bez = liesEingabe();
					// Die Bibliothek das Buch löschen lassen:
					shop.loescheArtikel(bez);
					break;
				case "ue":
					// lies die notwendigen Parameter, einzeln pro Zeile
					System.out.print("Username > ");
					username = liesEingabe();

					System.out.print("User Passwort  > ");
					password = liesEingabe();
					System.out.print("UserId  > ");
					nr = Integer.parseInt(liesEingabe());


					try {
						shop.fuegeMtiarbeiterEIn(username, password, nr);
						System.out.println("Einfügen ok");
					} catch (ArtikelExistiertBereitsException e) {
						// Hier Fehlerbehandlung...
						System.out.println("Fehler beim Einfügen");
						e.printStackTrace();
					}
					break;
				case "uf":
					System.out.print("Username  > ");
					bez = liesEingabe();
					tempUser = shop.sucheNachUser(bez);
					System.out.print(tempUser);
					break;
				case "log":
					System.out.print("Erfolgreich Ausgeloggt!");
					menueState = "login";
					gibLoginAus();
					break;
			}
				gibMitarbeiterMenueAus();
		}
		else if (menueState.equals("login")){
			switch (line) {

				case "e":
					System.out.print("Username  > ");
					username = liesEingabe();
					System.out.print("Passwort  > ");
					password = liesEingabe();
					try {
						user = shop.logIn(username, password);

						System.out.println("Du bist eingeloggt als " + user.getName());

						if (user instanceof Kunde) {
							menueState = "kunde";
							gibKundeMenueAus();
						} else if (user instanceof Mitarbeiter) {
							menueState = "mitarbeiter";
							gibMitarbeiterMenueAus();
						}
					} catch (LoginFehlgeschlagenException e) {
						System.err.println(e.getMessage());
						menueState = "main";
					}
					break;
				case "r":
					System.out.print("Username  > ");
					username = liesEingabe();
					System.out.print("Passwort  > ");
					password = liesEingabe();
					System.out.print("Usernummer  > ");
					int userNr = Integer.parseInt(liesEingabe());
					System.out.print("Straße  > ");
					String strasse = liesEingabe();
					System.out.print("PLZ  > ");
					String plz = liesEingabe();
					System.out.print("Wohnort  > ");
					String wohnort = liesEingabe();
					shop.fuegeUserEin(userNr, username, password,strasse,plz, wohnort);
					System.out.println("Benutzer wurde etstellt.");
					shop.schreibeDaten();
					gibLoginAus();

					break;

			}
		} else if (menueState.equalsIgnoreCase("warenkorb")) {
			switch (line) {
				case "a":
					gibWarenkorbAus(((Kunde) user).getWarenkorb());
					gibWarenkorbMenuAus();
					break;
				case "h":
					System.out.println("Welcher Artikel soll zum Warenkorb hinzugefügt werden?");
					bez = liesEingabe();
					liste = shop.sucheNachArtikel(bez);
					if (liste.size() == 1) {
						System.out.println("Anzahl des Artikels?");
						int quantity = liesEingabeInt();
						warenkorbVerwaltung.addArtikel(((Kunde) user).getWarenkorb(), liste.get(0), quantity);
					} else {
						System.out.println("Kein artikel mit dieser bezeichnung gefunden");
					}
					gibWarenkorbMenuAus();
					break;
				case "l":
					warenkorbVerwaltung.clear(((Kunde) user).getWarenkorb());
					System.out.println("Warenkorb wurde geleert!");
					gibWarenkorbMenuAus();
					break;
				case "m":
					System.out.println("Vom welchen artikel soll die Menge verändert werden?");
					bez = liesEingabe();
					System.out.println("Menge angeben");
					warenkorbVerwaltung.changeQuantity(((Kunde) user).getWarenkorb(), bez, liesEingabeInt());
					gibWarenkorbMenuAus();
					break;
				case "q":
					menueState = "kunde";
					gibKundeMenueAus();
					break;
				case "k":
					gibRechnungAus(shop.warenkorbKaufen(((Kunde) user)));
					gibArtikellisteAus(shop.gibAlleArtikel());
					menueState = "kunde";
					gibKundeMenueAus();
					break;
			}
		}

	}

	*//* (non-Javadoc)
	 * 
	 * Interne (private) Methode zum Ausgeben von Bücherlisten.
	 *
	 *//*
	private void gibRechnungAus(Rechnung r) {
		System.out.println(r.getDate());
		System.out.println("------Gekaufte Artikel-------");
		for (int i = 0; i < r.getArtikels().size(); i++) {
			System.out.println(r.getArtikels().get(i).getBezeichnung() + " x" + r.getQuantities().get(i) + " = " + r.getArtikels().get(i).getPreis() + " euro");
		}
		System.out.println("-------------------");
		System.out.println("Gesamt Preis: "+ r.calculatePrice());
		System.out.println("Kunde: "+ r.getKunde());
	}

	private void gibArtikellisteAus(List<Artikel> liste) {
		if (liste.isEmpty()) {
			System.out.println("Liste ist leer.");
		} else {
			for (Artikel artikel : liste) {
				System.out.println(artikel);
			}
		}
	}

	private void gibUserlisteAus(List<User> liste) {
		if (liste.isEmpty()) {
			System.out.println("Liste ist leer.");
		} else {
			for (User user : liste) {
				System.out.println(user);
			}
		}
	}

	private void gibWarenkorbAus(Warenkorb warenkorb) {
		System.out.println("Artikel Bezeichnung     |       Menge");
		if (warenkorb.getArtikelInCart().size() > 0) {
			for (Artikel a : warenkorb.getArtikelInCart()) {
				System.out.println(a.getBezeichnung() + "             x" + warenkorb.getQuantities().get(warenkorb.getArtikelInCart().indexOf(a)));
			}
		} else {
			System.out.println("Der Warenkorb ist leer");
		}


		System.out.println("-----------------------------------------");
	}

	*//**
	 * Methode zur Ausführung der Hauptschleife:
	 * - Menü ausgeben
	 * - Eingabe des Benutzers einlesen
	 * - Eingabe verarbeiten und Ergebnis ausgeben
	 * (EVA-Prinzip: Eingabe-Verarbeitung-Ausgabe)
	 *//*
	public void run() throws IOException {
		// Variable für Eingaben von der Konsole
		String input = ""; 
	
		// Hauptschleife der Benutzungsschnittstelle
		do {
			try {
				if (menueState.equals("main")) {
					gibLoginAus();
				}
				input = liesEingabe();
				verarbeiteEingabe(input);
			} catch (IOException | ArtikelExistiertBereitsException e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while (!input.equals("q"));
		shop.schreibeDaten();
	}

	
	*//**
	 * Die main-Methode...
	 *//*
	public static void main(String[] args) {
		ShopClientCUI cui;
		try {
			cui = new ShopClientCUI("SHOP");
			cui.run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//Login myApp= new Login();
		//myApp.showMenu();
	}*/
}
