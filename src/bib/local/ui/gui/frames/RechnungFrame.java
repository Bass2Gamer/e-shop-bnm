package bib.local.ui.gui.frames;

import bib.local.domain.Shop;
import bib.local.ui.gui.tablleModels.WarenkorbTableModel;
import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Rechnung;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class RechnungFrame extends JFrame {
    private Rechnung rechnung;
    private Shop shop;
    private JLabel kundenrT,kundewT, kundenT, totalT, dateT;

    private JTable table;
    private JScrollPane scroll;
    private JButton refresh, checkOut;

    public RechnungFrame(Rechnung rechnung, Shop shop) {
        this.rechnung = rechnung;
        this.shop = shop;

        JFrame frame = this;
        setSize(600, 600);
        setVisible(true);
        setLayout(null);

        kundenrT = new JLabel();
        kundenrT.setText("Kunden Nummer: " + rechnung.getKunde().getUserNr());
        kundenrT.setBounds(10 ,0, 500, 20);
        add(kundenrT);

        dateT = new JLabel();
        dateT.setText(rechnung.getDate() + "");
        dateT.setBounds(510 ,0, 1000, 20);
        add(dateT);

        kundenT = new JLabel();
        kundenT.setText(rechnung.getKunde().getName());
        kundenT.setBounds(10 ,20, 1000, 20);
        add(kundenT);

        kundewT = new JLabel();
        kundewT.setText(rechnung.getKunde().getStrasse() + " " + rechnung.getKunde().getPlz() + ", " + rechnung.getKunde().getWohnort());
        kundewT.setBounds(10 ,40, 1000, 20);
        add(kundewT);

        JLabel artikel = new JLabel();
        artikel.setText("Artikel: ");
        artikel.setBounds(10 ,70, 1000, 20);
        add(artikel);

        refresh = new JButton("Bestätigen");
        refresh.setBounds(180, 530,200, 30);
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                shop.warenkorbKaufen();
                dispose();
            }
        });

        table = new JTable(new WarenkorbTableModel(rechnung.getWarenkorb()));
        scroll = new JScrollPane(table);

        table.setBounds(10, 90, 590, 400);

        add(refresh);
        add(table);
        //add(refresh);
        table.add(scroll);

        JLabel gesamt = new JLabel();
        artikel.setText("Gesamtpreis: " + rechnung.calculatePrice());
        artikel.setBounds(440 ,500, 1000, 20);
        add(artikel);
    }
}
