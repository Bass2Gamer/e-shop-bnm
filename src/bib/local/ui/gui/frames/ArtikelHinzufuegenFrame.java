package bib.local.ui.gui.frames;

import bib.local.domain.Shop;
import bib.local.ui.gui.ShopClientGUI;
import bib.local.ui.gui.panels.ArtikelVerwaltungPanel;
import bib.local.ui.gui.tablleModels.ArtikelTableModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ArtikelHinzufuegenFrame extends JFrame {
    private Shop shop;
    private JTable table;

    public ArtikelHinzufuegenFrame(Shop shop, JTable table) {
        this.shop = shop;
        this.table = table;
        initialize();
    }

    public void initialize() {
        setLayout(null);
        setVisible(true);
        setSize(400, 350);

        JTextField einheit = new JTextField("einheit");
        einheit.setBounds(0, 120, 150, 20);
        einheit.setVisible(false);
        add(einheit);

        JCheckBox massengut = new JCheckBox("Massengutartikel");
        massengut.setBounds(160, 0, 250, 20);
        massengut.setVisible(true);
        massengut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                einheit.setVisible(massengut.isSelected());
            }
        });
        add(massengut);

        JTextField bezeichnung = new JTextField("bezeichnung");
        bezeichnung.setBounds(0, 0, 150, 20);
        add(bezeichnung);

        JTextField preis = new JTextField("preis");
        preis.setBounds(0, 40, 150, 20);
        add(preis);

        JTextField menge = new JTextField("menge");
        menge.setBounds(0, 80, 150, 20);
        add(menge);

        JFrame frame = this;
        JButton artikelHinzufügen = new JButton("Artikel hinzufügen");
        artikelHinzufügen.setBounds(0, 150, 100, 20);
        artikelHinzufügen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String artikelBezeichnung = bezeichnung.getText();
                    double artikelPreis = Double.parseDouble(preis.getText());
                    int artikelMenge = Integer.parseInt(menge.getText());

                    if (massengut.isSelected()) {
                        int artikelEinheit = Integer.parseInt(einheit.getText());
                        shop.fuegeArtikelEin(artikelBezeichnung, artikelMenge, artikelPreis, artikelEinheit);
                    } else {
                       shop.fuegeArtikelEin(artikelBezeichnung, artikelMenge, artikelPreis);
                    }
                    table.setModel(new ArtikelTableModel(shop.gibAlleArtikel()));
                    table.repaint();
                    shop.schreibeDaten();
                    dispose();
                } catch (Exception eee) {
                    JOptionPane.showMessageDialog(frame, "Operation konnte nicht durchgeführt werden. Es wurde keine gültige Zahl eingeben.");
                }
            }
        });
        add(artikelHinzufügen);

        repaint();
    }
}
