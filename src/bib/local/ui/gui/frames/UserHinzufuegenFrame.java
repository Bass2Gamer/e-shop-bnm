package bib.local.ui.gui.frames;

import bib.local.domain.Shop;
import bib.local.ui.gui.ShopClientGUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UserHinzufuegenFrame extends JFrame {
    private Shop shop;
    private String type;
    private ShopClientGUI gui;

    private LoginFrame loginFrame;

    public UserHinzufuegenFrame(Shop shop, ShopClientGUI gui, String type) {
        this.shop = shop;
        this.type = type;
        initialize();
        this.gui = gui;
    }

    public UserHinzufuegenFrame(Shop shop, String type) {
        this.shop = shop;
        this.type = type;
        initialize();
        this.gui = gui;
    }

    public void initialize() {
        setLayout(null);
        setVisible(true);
        setSize(400, 350);

        switch (type) {
            case "k":
                kundeDialouge();
                break;
            case "m":
                mitarbeiterDialouge();
                break;
        }

        repaint();
    }

    public void mitarbeiterDialouge() {
        JTextField bezeichnung = new JTextField("name");
        bezeichnung.setBounds(0, 0, 150, 20);
        add(bezeichnung);

        JTextField passwort = new JTextField("passwort");
        passwort.setBounds(0, 40, 150, 20);
        add(passwort);


        JButton mitarbeiteHinzu = new JButton("Mitarbeiter hinzufügen");
        mitarbeiteHinzu.setBounds(0, 150, 130, 20);
        mitarbeiteHinzu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = bezeichnung.getText();
                    String pass = passwort.getText();

                    shop.fuegeMtiarbeiterEIn(name, pass);

                    shop.schreibeDaten();
                    dispose();
                    //new LoginFrame(shop, gui);
                } catch (Exception eee) {
                    System.out.println(eee);
                }
            }
        });
        add(mitarbeiteHinzu);

        JButton cancel = new JButton("Cancel");
        cancel.setBounds(140, 150, 100, 20);
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                } catch (Exception eee) {
                    System.out.println(eee);
                }
            }
        });
        add(cancel);

    }

    public void kundeDialouge() {
        JTextField bezeichnung = new JTextField("name");
        bezeichnung.setBounds(0, 0, 150, 20);
        add(bezeichnung);

        JTextField passwort = new JTextField("passwort");
        passwort.setBounds(0, 40, 150, 20);
        add(passwort);

        JTextField straße = new JTextField("straße");
        straße.setBounds(0, 80, 150, 20);
        add(straße);

        JTextField plz = new JTextField("plz");
        plz.setBounds(0, 120, 150, 20);
        add(plz);

        JTextField wohnort = new JTextField("wohnort");
        wohnort.setBounds(0, 160, 150, 20);
        add(wohnort);


        JFrame frame = this;
        JButton addUser = new JButton("Registrieren");
        addUser.setBounds(0, 200, 100, 20);
        addUser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String name = bezeichnung.getText();
                    String pass = passwort.getText();
                    String strass = straße.getText();
                    String postl = plz.getText();
                    String wohn = wohnort.getText();

                    shop.fuegeUserEin(bezeichnung.getText(), straße.getText() ,plz.getText(), wohnort.getText(),passwort.getText());

                    shop.schreibeDaten();
                    dispose();
                    new LoginFrame(shop, gui);
                } catch (Exception eee) {
                    System.out.println(eee);
                    JOptionPane.showConfirmDialog(frame, eee.toString());
                }
            }
        });
        add(addUser);


        JButton cancel = new JButton("Cancel");
        cancel.setBounds(110, 200, 100, 20);
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    dispose();
                } catch (Exception eee) {
                    System.out.println(eee);
                }
            }
        });
        add(cancel);

    }
}
