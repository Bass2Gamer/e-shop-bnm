package bib.local.ui.gui.frames;

import bib.local.domain.Shop;
import bib.local.domain.exceptions.ArtikelUnterBestandException;
import bib.local.valueobjects.Artikel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ArtikelInWarenKorbFrame extends JFrame {
    private Shop shop;

    private Artikel artikel;

    private JButton addButton;
    private JButton cancellButton;
    private JTextField quantity;
    private JLabel currentItem;


    public ArtikelInWarenKorbFrame(Shop shop, Artikel currenArtikel) {
        this.shop = shop;
        this.artikel = currenArtikel;
        initialize();
    }

    public void initialize() {
        setSize(400, 300);
        setVisible(true);
        setLayout(null);

        JFrame frame = this;
        addButton = new JButton("Add");
        addButton.setBounds(200, 80, 100,30);
        addButton.setName("Hinzufügen");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    shop.artikelInWarenkorb(artikel, Integer.parseInt(quantity.getText()));
                } catch (ArtikelUnterBestandException ex) {
                    JOptionPane.showMessageDialog(frame, "Diese Menge an Artikeln kann nicht in den Warenkorb hinzugefügt werden.");
                }
                dispose();
            }
        });
        add(addButton);

        cancellButton = new JButton("Cancel");
        cancellButton.setBounds(200, 130, 100,30);
        cancellButton.setName("Cancel");
        cancellButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        add(cancellButton);

        currentItem = new JLabel("Artikel: " + artikel.getBezeichnung() + " ");
        currentItem.setText("Artikel: " + artikel.getBezeichnung() + " ");
        currentItem.setBounds(10, 50, 200,200);
        add(currentItem);

        quantity = new JTextField("");
        quantity.setBounds(0, 0, 200, 30);
        add(quantity);

        repaint();
    }

}
