package bib.local.ui.gui.frames;

import bib.local.domain.Shop;
import bib.local.domain.exceptions.LoginFehlgeschlagenException;
import bib.local.ui.gui.ShopClientGUI;
import bib.local.valueobjects.User;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class LoginFrame extends JFrame {
    private Shop shop;

    private ShopClientGUI shopClientGUI;
    private JButton logInButton;
    private JButton registerButton;
    private JTextField usernameFeld;
    private JTextField passwortFeld;

    private UserHinzufuegenFrame userHinzufuegenFrame;

    private String username = "", passwort = "";

    public LoginFrame(Shop shop, ShopClientGUI shopClientGUI) throws IOException {
        this.shopClientGUI = shopClientGUI;
        this.shop = shop;
        initialize();
    }

    public void initialize() {
        JFrame frame = this;
        setSize(400, 300);
        setVisible(true);
        setLayout(null);

        logInButton = new JButton("Log In");
        logInButton.setBounds(10, 140, 150,30);
        logInButton.setName("Log in");
        logInButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    shop.logIn(usernameFeld.getText(), passwortFeld.getText());
                    shopClientGUI.initialize();
                    dispose();
                } catch (LoginFehlgeschlagenException | IOException exception) {
                    JOptionPane.showMessageDialog(frame, exception.toString());
                }

            }
        });
        add(logInButton);

        JButton register = new JButton("Registrieren");
        register.setBounds(170, 140, 150,30);
        register.setName("Register");
        register.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userHinzufuegenFrame = new UserHinzufuegenFrame(shop, shopClientGUI, "k");
                dispose();

            }
        });
        add(register);

        usernameFeld = new JTextField("p");
        usernameFeld.setBounds(0, 0, 200, 30);
        add(usernameFeld);

        passwortFeld = new JTextField("p");
        passwortFeld.setBounds(0, 40, 200, 30);
        add(passwortFeld);

        repaint();

    }
}
