package bib.local.ui.gui.tablleModels;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Warenkorb;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class WarenkorbTableModel extends AbstractTableModel {

    private List<Artikel> artikel;
    private List<Integer> quantities;
    private String[] spaltenNamen = { "Bezeichnung", "Anzahl", "Preis" , "Presi Gesamt"};


    public WarenkorbTableModel(Warenkorb warenkorb) {
        super();
        artikel = new Vector<Artikel>();
        quantities = new Vector<Integer>();
        artikel.addAll(warenkorb.getArtikelInCart());
        quantities.addAll(warenkorb.getQuantities());
    }

    @Override
    public int getRowCount() {
        return artikel.size();
    }


    @Override
    public int getColumnCount() {
        return spaltenNamen.length;
    }

    @Override
    public String getColumnName(int col) {
        return spaltenNamen[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        Artikel selectedArtikel = artikel.get(row);
        int quantity = quantities.get(row);
        switch (col) {

            case 0:
                return selectedArtikel.getBezeichnung();
            case 1:
                return quantity + " stk.";
            case 2:
                return selectedArtikel.getPreis()+ " €";
            case 3:
                return "Gesamt: " + (selectedArtikel.getPreis() * quantity)+ " €";
            default:
                return null;
        }
    }
}
