package bib.local.ui.gui.tablleModels;

import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.User;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.Vector;

public class BenutzerTableModel extends AbstractTableModel {

    private List<User> user;
    private String[] spaltenNamen = { "ID","Name", "Typ"};


    public BenutzerTableModel(List<User> items) {
        super();
        user = new Vector<User>();
        user.addAll(items);
    }

    @Override
    public int getRowCount() {
        return user.size();
    }


    @Override
    public int getColumnCount() {
        return spaltenNamen.length;
    }

    @Override
    public String getColumnName(int col) {
        return spaltenNamen[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        User selectedUser = user.get(row);
        switch (col) {
            case 0:
                return selectedUser.getUserNr();
            case 1:
                return selectedUser.getName() ;
            case 2:
                return selectedUser.getType();

            default:
                return null;
        }
    }
}
