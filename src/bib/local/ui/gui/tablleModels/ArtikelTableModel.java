package bib.local.ui.gui.tablleModels;

import bib.local.valueobjects.Artikel;

import javax.swing.table.AbstractTableModel;
import java.util.List;
import java.util.Vector;

public class ArtikelTableModel extends AbstractTableModel {

    private List<Artikel> artikel;
    private String[] spaltenNamen = { "ID","Bezeichnung", "Bestand", "Preis"};


    public ArtikelTableModel(List<Artikel> items) {
        super();
        artikel = new Vector<Artikel>();
        artikel.addAll(items);
    }

    @Override
    public int getRowCount() {
        return artikel.size();
    }


    @Override
    public int getColumnCount() {
        return spaltenNamen.length;
    }

    @Override
    public String getColumnName(int col) {
        return spaltenNamen[col];
    }

    @Override
    public Object getValueAt(int row, int col) {
        Artikel gewaehltesBuch = artikel.get(row);
        switch (col) {
            case 0:
                return gewaehltesBuch.getNummer();
            case 1:
                return gewaehltesBuch.getBezeichnung();
            case 2:
                return gewaehltesBuch.getBestand() + " stk.";
            case 3:
                return gewaehltesBuch.getPreis() + " €";
            default:
                return null;
        }
    }
}
