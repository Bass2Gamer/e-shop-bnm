package bib.local.ui.gui;

import bib.local.domain.Shop;
import bib.local.ui.gui.frames.LoginFrame;
import bib.local.ui.gui.panels.*;
import bib.local.valueobjects.Kunde;
import bib.local.valueobjects.Mitarbeiter;

import javax.swing.*;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShopClientGUI extends JFrame{

    private Shop shop;
    private ShopClientGUI gui;


    private JTabbedPane tabbedPane;

   private ArtikelPanel artikelPanel;
   private ArtikelVerwaltungPanel artikelVerwaltungPanel;
   private BenutzerVerwaltungPanel benutzerVerwaltungPanel;
   private LoginFrame logInPanel;
   private WarenkorbPanel warenkorbPanel;



    public ShopClientGUI() {
        try {

            gui = this;
            shop = new Shop("SHOP");
            logInPanel = new LoginFrame(shop, gui);
           // initialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initialize() throws IOException {
        setSize(800, 600);
        setVisible(true);

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowCloser());

        setLayout(null);

        setupMenu();

        //remove(tabbedPane);
        tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(5, 5, 775, 525);

        add(tabbedPane);

           if(shop.currentUser instanceof Mitarbeiter) {
               setTitle("E-Shop BNM | Eingeloggt als Mitarbeiter");

                artikelVerwaltungPanel = new ArtikelVerwaltungPanel(shop);
                benutzerVerwaltungPanel = new BenutzerVerwaltungPanel(shop);

                tabbedPane.addTab("Artikel Verwaltung",artikelVerwaltungPanel);
                tabbedPane.addTab("Benutzer Verwaltung",benutzerVerwaltungPanel);


           } else if (shop.currentUser instanceof Kunde) {
               setTitle("E-Shop BNM | Eingeloggt als Kunde");
               artikelPanel = new ArtikelPanel(shop);
               warenkorbPanel = new WarenkorbPanel(shop);

               tabbedPane.addTab("Artikel" , artikelPanel);
               tabbedPane.addTab("Warenkorb", warenkorbPanel);


           } else {
           }

    }

    private void setupMenu() {
        JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new FileMenu();
        JMenu helpMenu = new HelpMenu();
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        setJMenuBar(menuBar);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ShopClientGUI gui = new ShopClientGUI();
            }
        });
    }

    class FileMenu extends JMenu implements ActionListener {

        public FileMenu() {
            super("File");

            JMenuItem saveItem = new JMenuItem("Save");
            saveItem.addActionListener(this);
            add(saveItem);

            JMenuItem logoutItem = new JMenuItem("Logout");
            logoutItem.addActionListener(this);
            add(logoutItem);

            addSeparator();
            JMenuItem quitItem = new JMenuItem("Quit");
            quitItem.addActionListener(this);
            add(quitItem);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String actionCommand = e.getActionCommand();
            switch (actionCommand) {
                case "Save":
                    try {
                        shop.schreibeDaten();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    break;
                case "Quit":
                    setVisible(false);
                    dispose();
                    System.exit(0);
                    break;
                case "Logout":
                    try {
                        gui.remove(tabbedPane);
                        logInPanel = new LoginFrame(shop, gui);
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    dispose();
                    break;
            }
        }
    }

    class HelpMenu extends JMenu implements ActionListener {

        public HelpMenu() {
            super("Help");

            // Nur zu Testzwecken: Menü mit Untermenü
            JMenu m = new JMenu("About");
            JMenuItem mi = new JMenuItem("Programmers");
            mi.addActionListener(this);
            m.add(mi);
            mi = new JMenuItem("Stuff");
            mi.addActionListener(this);
            m.add(mi);
            this.add(m);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Klick auf Menü '" + e.getActionCommand() + "'.");
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*    private static LogInPanel logInWindow;
    protected   String type = "";
    private  String mitarbeiter = "mitarbeiter";
    private  String KUNDE = "kunde";

    JComponent artikel = new JPanel();
    JComponent warenkorb = new JPanel();
    JTable table = new JTable(new ArtikelTableModel(LogInPanel.shop.gibAlleArtikel()));



    public ShopClientGUI(String type) {
        System.out.println(LogInPanel.shop.gibAlleArtikel());
        this.type = type;
        initilaze();
    }

    private void gibArtikellisteAus(List<Artikel> liste) {
        if (liste.isEmpty()) {
            System.out.println("Liste ist leer.");
        } else {
            for (Artikel artikel : liste) {
                System.out.println(artikel);
            }
        }
    }

    public void initilaze() {
        setSize(800, 600);
        setVisible(true);
        setupMenu();
        //setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(0, 20, 760, 500);
        JComponent artikelVerwaltung = new JPanel();
        JComponent userVerwaltung = new JPanel();


        switch (type) {
            case "mitarbeiter":

                tabbedPane.addTab("Artikel Verwaltung", artikelVerwaltung);
                tabbedPane.addTab("User Verwaltung", userVerwaltung);
                setTitle("E-Shop BNM | Eingeloggt als Mitarbeiter");
                System.err.println(type);
            break;
            case "kunde":
                refreshList();

                setTitle("E-Shop BNM | Eingeloggt als Kunde");
                tabbedPane.addTab("Artikel", artikel);
                tabbedPane.addTab("Warenkorb", warenkorb);
                System.err.println(type);
                break;

            default:
                System.err.println(type);
                break;

        }
        setLayout(null);
        add(tabbedPane);
//


//        JButton warenKorb = new JButton("Warenkorb anzeigen");
//        warenKorb.setBounds(600, 500, 50, 20);
//        warenKorb.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//            }
//        });
//        artikel.add(warenKorb);
//



        repaint();
    }

    public void refreshWarenkorb() {
        warenkorb.removeAll();
        for (int i = 0; i < ((Kunde) LogInPanel.user).getWarenkorb().getArtikelInCart().size(); i++) {
            ArtikelPanel artikelObj = new ArtikelPanel(this, ((Kunde) LogInPanel.user).getWarenkorb().getArtikelInCart().get(i), i, ((Kunde) LogInPanel.user));
            warenkorb.add(artikelObj);
            repaint();
        }
    }

    public void refreshList() {
        artikel.removeAll();
        ShopClientGUI ref = this;
        JButton artikelHinzufügenButton = new JButton("Artikel hinzufügen");
        artikelHinzufügenButton.setBounds(0, 300, 100, 20);
        artikelHinzufügenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ArtikelHinzufuegenPanel a = new ArtikelHinzufuegenPanel(ref);
            }
        });
        artikel.add(artikelHinzufügenButton);
        
        artikel.add(table);
//        for (int i = 0; i < LogIn.shop.gibAlleArtikel().size(); i++) {
//            ArtikelPanel artikelObj = new ArtikelPanel(this, LogIn.shop.gibAlleArtikel().get(i), i, ((Kunde) LogIn.user));
//            artikel.add(artikelObj);
//        }
        repaint();
    }*/


}
