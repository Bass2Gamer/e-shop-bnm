package bib.local.ui.gui.components;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class ButtonRenderer implements TableCellRenderer {

    JButton button = new JButton();

    public Component getTableCellRendererComponent(JTable table, Object value,boolean isSelected, boolean hasFocus, int row, int column) {
        table.setShowGrid(true);
        table.setGridColor(Color.LIGHT_GRAY);
        button.setText(value.toString());
        button.setToolTipText("Press " + value.toString());
        return button;
    }
}