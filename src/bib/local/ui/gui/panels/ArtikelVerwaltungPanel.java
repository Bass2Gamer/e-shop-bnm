package bib.local.ui.gui.panels;

import bib.local.domain.Shop;
import bib.local.ui.gui.frames.ArtikelHinzufuegenFrame;
import bib.local.ui.gui.tablleModels.ArtikelTableModel;
import bib.local.valueobjects.Artikel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;
import java.util.Vector;

public class ArtikelVerwaltungPanel extends JPanel {

        private Shop shop;
        private Vector<Artikel> artikel;

        private JTable table;
        private AbstractTableModel myModel;
        private JScrollPane scroll;
        private JButton addArtikel, deleteArtikel;

        private ArtikelVerwaltungPanel panel;


        private ArtikelHinzufuegenFrame artikelHinzufuegenFrame;


        public ArtikelVerwaltungPanel(Shop shop) {
            this.shop = shop;
            this.panel = this;
            initialize();
        }

        public void initialize() {

            setLayout(null);
            myModel = new ArtikelTableModel(shop.gibAlleArtikel());
            table = new JTable(myModel);
            scroll = new JScrollPane(table);
            addArtikel = new JButton("Artikel Hinzfügen");
            addArtikel.setBounds(565, 10,200, 30);
            addArtikel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    artikelHinzufuegenFrame = new ArtikelHinzufuegenFrame(shop, table);

                }
            });
            deleteArtikel = new JButton("Artikel Läschen");
            deleteArtikel.setBounds(355, 10,200, 30);
            deleteArtikel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    UUID uuid = UUID.fromString(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

                    int selected = JOptionPane.showConfirmDialog(null,
                            "Willst du Wirklich " +shop.sucheNachArtikel(uuid).getBezeichnung() + " Löschen?",
                            "Artikel LLöschen"  ,
                            JOptionPane.YES_NO_OPTION);
                    if (selected == 0 ) {
                        shop.loescheArtikel(uuid);
                    }
                    table.setModel(new ArtikelTableModel(shop.gibAlleArtikel()));

                }
            });
            table.setBounds(10, 50, 755, 455);


            add(table);
            add(addArtikel);

            add(deleteArtikel);
            table.add(scroll);

            repaint();
        }
}
