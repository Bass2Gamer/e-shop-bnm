package bib.local.ui.gui.panels;

import bib.local.domain.Shop;
import bib.local.ui.gui.frames.RechnungFrame;
import bib.local.ui.gui.tablleModels.WarenkorbTableModel;
import bib.local.valueobjects.Artikel;
import bib.local.valueobjects.Kunde;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;
import java.util.Vector;

public class WarenkorbPanel extends JPanel {

    private Shop shop;
    private Vector<Artikel> artikel;

    private JTable table;
    private JScrollPane scroll;
    private JButton refresh, checkOut;

    public WarenkorbPanel(Shop shop) {
        this.shop = shop;
        initialize();
    }


    public void initialize() {

        setLayout(null);
        table = new JTable(new WarenkorbTableModel(shop.getWarenkorb()));
        scroll = new JScrollPane(table);
        refresh = new JButton("Aktualisieren");
        refresh.setBounds(0, 10,200, 30);
        refresh.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                table.setModel(new WarenkorbTableModel(shop.getWarenkorb()));
                table.repaint();
                System.out.println(shop.getWarenkorb().getArtikelInCart().size());
            }
        });
        table.setBounds(10, 50, 755, 455);


        add(table);
        add(refresh);
        table.add(scroll);

        checkOut = new JButton("Warenkorb kaufen");
        checkOut.setBounds(545, 10, 200, 30);
        checkOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RechnungFrame(shop.warenKorbRechnungErstellen( (Kunde) shop.currentUser), shop );
            }
        });
        add(checkOut);

        repaint();
    }
}
