package bib.local.ui.gui.panels;

import bib.local.domain.Shop;
import bib.local.ui.gui.frames.ArtikelHinzufuegenFrame;
import bib.local.ui.gui.frames.UserHinzufuegenFrame;
import bib.local.ui.gui.tablleModels.BenutzerTableModel;
import bib.local.valueobjects.Artikel;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;
import java.util.Vector;

public class BenutzerVerwaltungPanel extends JPanel {

    private Shop shop;
    private Vector<Artikel> artikel;

    private JTable table;
    private AbstractTableModel myModel;
    private JScrollPane scroll;
    private JButton addMitarb, deleteMitarb;

    private BenutzerVerwaltungPanel panel;


    private UserHinzufuegenFrame userHinzufuegenFrame;


    public BenutzerVerwaltungPanel(Shop shop) {
        this.shop = shop;
        this.panel = this;
        initialize();
    }

    public void initialize() {

        setLayout(null);
        myModel = new BenutzerTableModel(shop.getAllUser());
        table = new JTable(myModel);
        scroll = new JScrollPane(table);
        addMitarb = new JButton("Mitarbeiter Hinzufügen");
        addMitarb.setBounds(565, 10,200, 30);
        addMitarb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userHinzufuegenFrame = new UserHinzufuegenFrame(shop, "m");
                myModel.fireTableDataChanged();
                table.revalidate();
            }
        });
        deleteMitarb = new JButton("Mitarbeiter Läschen");
        deleteMitarb.setBounds(355, 10,200, 30);
        deleteMitarb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UUID uuid = UUID.fromString(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());

                int selected = JOptionPane.showConfirmDialog(null,
                        "Willst du Wirklich " +shop.sucheNachUser(uuid).getName() + " Löschen?",
                        "Mitarbeiter LLöschen"  ,
                        JOptionPane.YES_NO_OPTION);
                if (selected == 0 ) {
                    shop.loescheBenutzer(uuid);
                }
                myModel.fireTableDataChanged();
                table.revalidate();
            }
        });
        table.setBounds(10, 50, 755, 455);


        add(table);
        add(addMitarb);

        add(deleteMitarb);
        table.add(scroll);

        repaint();
    }
}
