package bib.local.ui.gui.panels;

import bib.local.domain.Shop;
import bib.local.ui.gui.frames.ArtikelInWarenKorbFrame;
import bib.local.ui.gui.tablleModels.ArtikelTableModel;
import bib.local.valueobjects.Artikel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.UUID;
import java.util.Vector;

public class ArtikelPanel extends JPanel {
    private Shop shop;
    private Vector<Artikel> artikel;

    private JTable table;
    private JScrollPane scroll;
    private JButton addArtikel;

    private ArtikelInWarenKorbFrame artikelInWarenKorbFrame;


    public ArtikelPanel(Shop shop) {
        this.shop = shop;
        initialize();
    }

    public void initialize() {
        JPanel panel = this;
        setLayout(null);
        table = new JTable(new ArtikelTableModel(shop.gibAlleArtikel()));
        scroll = new JScrollPane(table);
        addArtikel = new JButton("Zu Warenkorb Hinzufügen");
        addArtikel.setBounds(565, 10,200, 30);
        addArtikel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    UUID uuid = UUID.fromString(table.getModel().getValueAt(table.getSelectedRow(), 0).toString());
                    artikelInWarenKorbFrame = new ArtikelInWarenKorbFrame(shop, shop.sucheNachArtikel(uuid));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(panel, "Operation konnte nicht durchgeführt werden. Es wurde kein egültige Zahl eingegeben.");
                }

            }
        });
                table.setBounds(10, 50, 755, 455);


        add(table);
        add(addArtikel);
        table.add(scroll);

        repaint();
    }
}

