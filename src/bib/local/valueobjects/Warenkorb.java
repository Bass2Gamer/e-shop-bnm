package bib.local.valueobjects;

import java.util.ArrayList;

public class Warenkorb {
    private ArrayList<Artikel> artikels = new ArrayList<Artikel>();
    private ArrayList<Integer> quantities = new ArrayList<Integer>();

    public Warenkorb() {

    }

    public ArrayList<Artikel> getArtikelInCart() {
        return artikels;
    }

    public ArrayList<Integer> getQuantities() {
        return quantities;
    }

    public void setArtikels(ArrayList<Artikel> artikels) {
        this.artikels = artikels;
    }

    public void setQuantities(ArrayList<Integer> quantities) {
        this.quantities = quantities;
    }
}
