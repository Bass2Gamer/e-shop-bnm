package bib.local.valueobjects;

public class Massengutartikel extends Artikel{
    int einheit = 0;


    //Nur in einheit hinzufügen können
    public Massengutartikel(String titel,double preis ,int bestand, int einheit) {
        super(titel, preis, bestand);

        this.einheit = einheit;
    }

    public String getData() {
        String tempString =  bezeichnung +"#"+nummer +"#"+ preis +"#"+ bestand + "#" + einheit;
        return tempString;
    }
}
