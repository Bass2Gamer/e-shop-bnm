package bib.local.valueobjects;

import java.util.UUID;

/**
 * Klasse zur Repräsentation einzelner Kunden.
 *
 *
 * @author Mohamad Alkasem
 */
public class Kunde extends User{

	private String strasse = "";
	private String plz = "";
	private String wohnort = "";


	public Kunde(String name, String password, String strasse, String plz, String wohnort) {
		super(name, password);
		this.strasse = strasse;
		this.plz = plz;
		this.wohnort = wohnort;
		type = "Kunde";
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getWohnort() {
		return wohnort;
	}

	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}

	public String getData() {

		String tempString = "" + userNr + "#" + name +"#" + password +"#" + strasse +"#"+ plz + "#"  + wohnort;
		return tempString;
	}
}
