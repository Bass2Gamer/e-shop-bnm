package bib.local.valueobjects;


import java.util.UUID;

/**
 * Klasse zur Repräsentation einzelner Artikeln
 * 
 * @author Mohamad Alkasem
 */
public class Artikel {

	// Attribute zur Beschreibung eines Artikels:
	protected String bezeichnung;
	protected UUID nummer;
	protected int bestand;
	protected double preis;

	/**
	 * blic Artikel(String titel, int nr) {
	 * this(titel, nr, 1);
	 * }
	 */

	public Artikel(String titel, double preis ,int bestand) {
		nummer = UUID.randomUUID();
		this.bezeichnung = titel;
		this.bestand = bestand;
		this.preis = preis;
	}
// --- Dienste der Artikel-Objekte ---

	/**
	 * Standard-Methode von Object überschrieben.
	 * Methode wird immer automatisch aufgerufen, wenn ein Artiekel-Objekt als String
	 * benutzt wird (z.B. in println(artikel);)
	 *
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		//	String verfuegbarkeit = bestand ? "verfuegbar" : "ausverkauft";

		return ("Nr: " + nummer + " | Bezeichnung: " + bezeichnung + " | Preis: " + preis + " | Bestand: " + bestand);
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}

	public int getBestand() {
		return bestand;
	}

	/**
	 * Standard-Methode von Object überschrieben.
	 * Methode dient Vergleich von zwei Artikel-Objekten anhand ihrer Werte,
	 * d.h. Titel und Nummer.
	 *
	 * @see java.lang.Object#toString()
	 * @return
	 */

	/*
	 * Ab hier Accessor-Methoden
	 */

	public UUID getNummer() {
		return nummer;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public boolean isVerfuegbar() {
		return bestand > 0;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public String getData() {
		String tempString =  bezeichnung +"#"+nummer +"#"+ preis +"#"+ bestand ;
		return tempString;
	}

}
