package bib.local.valueobjects;

import java.time.LocalDate;
import java.util.ArrayList;

public class Rechnung {
    private Kunde k;
    private LocalDate date;
    private Warenkorb warenkorb;

    public Rechnung(Kunde k, Warenkorb w) {
        this.k = k;
        this.warenkorb = new Warenkorb();
        this.warenkorb.setArtikels(w.getArtikelInCart());
        this.warenkorb.setQuantities(w.getQuantities());
        date = LocalDate.now(); // Create a date object
    }

    public double calculatePrice() {
        double price = 0;
        for (int i = 0; i < warenkorb.getArtikelInCart().size(); i++) {
            price += warenkorb.getArtikelInCart().get(i).getPreis() * warenkorb.getQuantities().get(i);
        }
        return price;
    }

    public LocalDate getDate() {
        return date;
    }

    public Kunde getKunde() {
        return k;
    }

    public ArrayList<Artikel> getArtikels() {
       return warenkorb.getArtikelInCart();
    }

    public Warenkorb getWarenkorb() {
        return warenkorb;
    }

    public ArrayList<Integer> getQuantities() {
        return warenkorb.getQuantities();
    }
}
