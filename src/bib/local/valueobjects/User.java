package bib.local.valueobjects;

import java.util.UUID;

/**
 * Klasse zur Repräsentation einzelner User.
 *
 *
 * @author Mohamad Alkasem
 */
public class User {
// Atributte der einezelner User
    protected UUID userNr;
    protected String name;
    protected String type;
  //  private float umsatz = 0.0f;

    protected String password;
// Konstraktor der Eigenschaften der User

    public User( String name, String password) {
        this.userNr = UUID.randomUUID();
        this.name = name;
        this.password = password;
    }

    // Methoden zum Setzen und Lesen der Kunden-Eigenschaften,
    // z.B. getStrasse() und setStrasse()

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getUserNr() { return userNr; }

    public void setUserNr(UUID userNr) {
        this.userNr = userNr;
    }

    public String getPassword() { return password; }

    public void setPassword(String password) {
        this.password = password;
    }

    public String toString() {
        //	String verfuegbarkeit = bestand ? "verfuegbar" : "ausverkauft";

        return ("UserId " + userNr + " / Username: " + name );
    }

    public String getData() {
        String tempString = userNr +"#"+  name+"#"+ password;
        return tempString;
    }

    public String getType() {
        return type;
    }
}


